from flask import Flask, render_template, request, redirect, url_for, jsonify
import random
app = Flask(__name__)


books = [{'title': 'Software Engineering', 'id': '1'}, \
		 {'title': 'Algorithm Design', 'id':'2'}, \
		 {'title': 'Python', 'id':'3'}]
         
    
@app.route('/book/JSON')

def bookJSON():
    return jsonify(books)

@app.route('/')
@app.route('/book/', methods = ['GET', 'POST'])
def showBook():
    return render_template('showBook.html', books = books) # Passing the dictionary to the html
	
@app.route('/book/new/', methods=['GET', 'POST'])
def newBook():
    if request.method == "POST": 
        new_book_name = request.form['name']
        new_id = len(books) + 1
        # Add the new book
        books.append({'title':new_book_name, 'id':str(new_id)})
        return redirect(url_for('showBook', books = books))
    else: 
        return render_template('newBook.html') 

@app.route('/book/<int:book_id>/edit/', methods=['GET','POST'])
def editBook(book_id):
    if request.method == "POST":
        new_name = request.form['name']
        for i in books: 
            if i['id'] == str(book_id): 
                i['title'] = new_name
        return redirect(url_for('showBook', books = books))
        
    else: 
        return render_template('editBook.html', book_id = book_id) # This is how you pass a dynamic component to Jinja
	
@app.route('/book/<int:book_id>/delete/', methods = ['GET', 'POST'])
def deleteBook(book_id):
    if request.method == "POST": 
        counter = 1
        for i in books: 
        
            # If it is the last book
            if i['id'] == str(book_id) and int(book_id) == len(books):
                # Delete the book without reseting index
                del books[counter -1]
                break
                
            # If it is not the last book
            elif i['id'] == str(book_id): 
                # Delete the book and reset index
                del books[counter -1]
                
                for j in range(counter -1, len(books)): 
                    books[j]['id'] = str(int(books[j]['id']) - 1)
                break
            counter += 1
        
        return redirect(url_for('showBook', books = books))
    else:  
        return render_template('deleteBook.html', book_id = book_id, books = books)

if __name__ == '__main__':
	app.debug = True
	app.run(host = '0.0.0.0', port = 5000)
	

